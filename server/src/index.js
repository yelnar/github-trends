const express = require('express');
const fetch = require('node-fetch');
const cors = require('cors');

const app = express();
const port = process.env.PORT || 4000;

app.use(cors());

console.log(__dirname);

const router = express.Router();

router.route('/repositories').get((req, res) => {
  fetch('https://gh-trending-api.herokuapp.com/repositories')
    .then((response) => {
      response
        .json()
        .then((data) => {
          res.json(data);
        })
        .catch((error) => {
          res.send(error);
        });
    })
    .catch((error) => {
      res.send(error);
    });
});

router.route('/developers').get((req, res) => {
  fetch('https://gh-trending-api.herokuapp.com/developers')
    .then((response) => {
      response
        .json()
        .then((data) => {
          res.json(data);
        })
        .catch((error) => {
          res.send(error);
        });
    })
    .catch((error) => {
      res.send(error);
    });
});

app.use('/api', router);

app.listen(port, () => {
  console.log(`Server listening at ${port}`);
});
