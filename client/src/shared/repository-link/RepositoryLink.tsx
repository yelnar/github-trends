import React from 'react';
import styled from 'styled-components';

import { RepoIcon } from '../../components/icons';
import { ICommonProps } from '../../components/common';

interface IRepositoryLinkProps extends ICommonProps {
  name: string;
  url: string;
}

export const RepositoryLink: React.FC<IRepositoryLinkProps> = ({
  className,
  name,
  url,
}) => {
  return (
    <Root className={className} href={url} target={'_blank'}>
      <RepoIcon /> {name}
    </Root>
  );
};

const Root = styled.a`
  display: flex;
  align-items: center;
  justify-content: center;
  font-size: 1.25rem;
  font-weight: 400;
  color: ${({ theme }) => theme.colors.primary[1]};
  text-decoration: none;

  :hover {
    text-decoration: underline;
  }

  ${RepoIcon} {
    margin: 0.25rem 0.5rem 0 0;
  }
`;
