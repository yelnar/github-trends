import React from 'react';
import styled from 'styled-components';
import { Link, Routes, Route, Navigate } from 'react-router-dom';

import { Repositories } from '../../routes/repositories/Repositories';
import { Developers } from '../../routes/developers/Developers';

export const Trends: React.FC = () => {
  return (
    <Root>
      <BoxHeader>
        <StyledLink to="/repositories">Repositories</StyledLink>
        <StyledLink to="/developers">Developers</StyledLink>
      </BoxHeader>

      <Routes>
        <Route path="/" element={<Navigate replace to="/repositories" />} />
        <Route path="repositories" element={<Repositories />} />
        <Route path="developers" element={<Developers />} />
      </Routes>
    </Root>
  );
};

const Root = styled.div`
  margin: 0 12rem;
  padding: 3rem 1rem;
  display: flex;
  align-items: center;
  justify-content: center;
  flex-direction: column;
`;

const BoxHeader = styled.div`
  width: 100%;
  padding: 1rem;
  display: flex;
  align-items: center;
  background-color: ${({ theme }) => theme.colors.dark[2]};
  border: 1px solid ${({ theme }) => theme.colors.dark[3]};
  border-radius: ${({ theme }) => theme.layout.borderRadius};
  border-bottom-left-radius: 0;
  border-bottom-right-radius: 0;
`;

const StyledLink = styled(Link)`
  font-size: 1rem;
  line-height: 1.25;
  font-weight: 500;
  color: ${({ theme }) => theme.colors.primary[1]};
  text-decoration: none;
  margin-right: 1rem;

  :hover {
    text-decoration: underline;
  }
`;
