import React from 'react';
import styled from 'styled-components';

import { ICommonProps } from '../../components/common';

interface ILoadingProps extends ICommonProps {}

export const Loading: React.FC<ILoadingProps> = ({ className }) => {
  return <Root className={className}>Loading...</Root>;
};

const Root = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  width: 100%;
  margin: 0;
  border: 1px solid ${({ theme }) => theme.colors.dark[3]};
  padding: 3rem 1rem;
  border-top-color: transparent;
`;
