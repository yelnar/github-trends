import React from 'react';
import styled from 'styled-components';

import { Loading } from '../../shared/loading/Loading';

import { IRepository, RepositoryCard } from './RepositoryCard';
import { fetchRepositories } from '../../api';

interface IRepositoriesProps {}

export const Repositories: React.FC<IRepositoriesProps> = () => {
  const [repositories, setRepositories] = React.useState<
    IRepository[] | undefined
  >(undefined);

  React.useEffect(() => {
    let isMounted = true;

    const getRepositories = async () => {
      const repositories = await fetchRepositories();
      if (repositories && isMounted) {
        setRepositories(repositories);
      }
    };

    getRepositories();

    return () => {
      isMounted = false;
    };
  }, []);

  if (!repositories) {
    return <Loading />;
  }

  return (
    <Root>
      {repositories.map((repository) => (
        <RepositoryCard key={repository.url} {...repository} />
      ))}
    </Root>
  );
};

const Root = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  flex-direction: column;
`;
