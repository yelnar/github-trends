import React from 'react';
import styled from 'styled-components';

import { StarIcon, ForkIcon } from '../../components/icons';
import { RepositoryLink } from '../../shared/repository-link/RepositoryLink';

interface IMaintainer {
  username: string;
  url: string;
  avatar: string;
}

export interface IRepository {
  repositoryName: string;
  description?: string;
  language: string;
  url: string;
  totalStars: number;
  starsSince: number;
  forks: number;
  builtBy: IMaintainer[];
}

interface ITrendingRepositoryCardProps extends IRepository {}

export const RepositoryCard: React.FC<ITrendingRepositoryCardProps> = ({
  repositoryName,
  description,
  language,
  url,
  totalStars,
  starsSince,
  forks,
  builtBy,
}) => {
  return (
    <Root>
      <TopRow>
        <RepositoryLink name={repositoryName} url={url} />
      </TopRow>

      {description ? <Description>{description}</Description> : null}

      <BottomRow>
        <BottomRowLeft>
          {language ? <Info>{language}</Info> : null}

          <Info>
            <StarIcon /> {totalStars}
          </Info>

          <Info>
            <ForkIcon /> {forks}
          </Info>

          {builtBy.length ? (
            <Info>
              Built by
              {builtBy.map((developer) => (
                <Avatar key={developer.username} src={developer.avatar} />
              ))}
            </Info>
          ) : null}
        </BottomRowLeft>

        <BottomRowRight>
          {starsSince ? (
            <Info>
              <StarIcon /> {starsSince}
            </Info>
          ) : null}
        </BottomRowRight>
      </BottomRow>
    </Root>
  );
};

const Root = styled.article`
  display: flex;
  flex-direction: column;
  width: 100%;
  padding: 1rem;
  border: 1px solid ${({ theme }) => theme.colors.dark[3]};
  border-top-color: transparent;
`;

const TopRow = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;
`;

const Description = styled.p`
  display: flex;
  font-size: 0.875rem;
  margin: 0.5rem 0 0 0;
`;

const BottomRow = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;
  margin-top: 0.5rem;
`;

const BottomRowLeft = styled.div`
  display: flex;
`;

const Info = styled.span`
  display: flex;
  align-items: center;
  justify-content: center;
  font-size: 0.75rem;
  margin-left: 0;
  margin-right: 1rem;

  ${ForkIcon} {
    margin: 0 0.25rem 0 0;
  }

  ${StarIcon} {
    margin: 0 0.25rem 0 0;
  }
`;

const Avatar = styled.img`
  border-radius: 50%;
  display: inline-block;
  overflow: hidden;
  vertical-align: middle;
  width: 1.25rem;
  height: 1.25rem;
  margin: 0.2rem;
  box-shadow: 0 0 0 1px rgba(240, 246, 252, 0.1);
`;

const BottomRowRight = styled.div`
  display: flex;

  ${Info} {
    margin-right: 0;
  }
`;
