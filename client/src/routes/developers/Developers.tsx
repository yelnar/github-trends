import React from 'react';
import styled from 'styled-components';

import { Loading } from '../../shared/loading/Loading';

import { IDeveloper, DeveloperCard } from './DeveloperCard';
import { fetchDevelopers } from '../../api/fetchDevelopers';

interface IDevelopersProps {}

export const Developers: React.FC<IDevelopersProps> = () => {
  const [developers, setDevelopers] = React.useState<IDeveloper[] | undefined>(
    undefined
  );

  React.useEffect(() => {
    let isMounted = true;

    const getDevelopers = async () => {
      const developers = await fetchDevelopers();
      if (developers && isMounted) {
        setDevelopers(developers);
      }
    };

    getDevelopers();

    return () => {
      isMounted = false;
    };
  }, []);

  if (!developers) {
    return <Loading />;
  }

  return (
    <Root>
      {developers.map((developer) => (
        <DeveloperCard key={developer.username} {...developer} />
      ))}
    </Root>
  );
};

const Root = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  flex-direction: column;
  width: 100%;
`;
