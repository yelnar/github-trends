import React from 'react';
import styled from 'styled-components';

import { FlameIcon } from '../../components/icons';
import { RepositoryLink } from '../../shared/repository-link/RepositoryLink';

interface IPopularRepository {
  description: string;
  repositoryName: string;
  url: string;
}

export interface IDeveloper {
  avatar: string;
  name: string;
  rank: number;
  url: string;
  username: string;
  popularRepository: IPopularRepository;
}

interface IDeveloperCardProps extends IDeveloper {}

export const DeveloperCard: React.FC<IDeveloperCardProps> = ({
  avatar,
  name,
  rank,
  url,
  username,
  popularRepository,
}) => {
  return (
    <Root>
      <Rank>{rank}</Rank>

      <Avatar src={avatar} />

      <Container>
        <UserInfo>
          <DeveloperName href={url} target={'_blank'}>
            {name}
          </DeveloperName>
          <DeveloperUsername>{username}</DeveloperUsername>
        </UserInfo>
      </Container>

      <Container>
        {popularRepository.url ? (
          <PopularRepo>
            <div>
              <FlameIcon /> Popular Repo
            </div>

            <RepositoryLinkStyled
              name={popularRepository.repositoryName}
              url={popularRepository.url}
            />
          </PopularRepo>
        ) : null}
      </Container>
    </Root>
  );
};

const Root = styled.article`
  display: flex;
  align-items: flex-start;
  width: 100%;
  padding: 1rem;
  border: 1px solid ${({ theme }) => theme.colors.dark[3]};
  border-top-color: transparent;
`;

const Rank = styled.a`
  font-size: 0.75rem;
`;

const DeveloperName = styled.a`
  font-size: 1.25rem;
  line-height: 1.25;
  font-weight: 600;
  color: ${({ theme }) => theme.colors.primary[1]};
  text-decoration: none;

  :hover {
    text-decoration: underline;
  }
`;

const DeveloperUsername = styled.a`
  font-size: 1rem;
  line-height: 1.25;
  font-weight: 400;
  text-decoration: none;

  :hover {
    text-decoration: underline;
    color: ${({ theme }) => theme.colors.primary[1]};
  }
`;

const Container = styled.div`
  width: 33%;
`;

const UserInfo = styled.div`
  display: flex;
  flex-direction: column;
`;

const PopularRepo = styled.div`
  display: flex;
  align-items: flex-start;
  flex-direction: column;

  div:first-child {
    display: flex;
    align-items: center;
    text-transform: uppercase;
    font-size: 0.75rem;
    color: #8b949e;

    ${FlameIcon} {
      fill: #db6d28;
      margin-right: 0.25rem;
    }
  }
`;

const RepositoryLinkStyled = styled(RepositoryLink)`
  font-size: 1rem;
`;

const Avatar = styled.img`
  border-radius: 50%;
  display: inline-block;
  overflow: hidden;
  vertical-align: middle;
  width: 3rem;
  height: 3rem;
  margin: 0 1rem;
  box-shadow: 0 0 0 1px rgba(240, 246, 252, 0.1);
`;
