import { css } from 'styled-components';

export const ThemeGlobalStyle = css`
  html {
    //font-variant-ligatures: none;
    //-webkit-font-variant-ligatures: none;
    //text-rendering: optimizeLegibility;
    //-moz-osx-font-smoothing: grayscale;
    //font-smoothing: antialiased;
    //-webkit-font-smoothing: antialiased;
    font-size: 16px;
  }

  body {
    font-family: ${({ theme }) => theme.fonts.primary};
    font-size: 1rem;
    line-height: 1.2;
    background: ${({ theme }) => theme.colors.dark[1]};
    color: ${({ theme }) => theme.colors.gray[1]};
  }

  html,
  body {
    height: 100%;
  }
`;
