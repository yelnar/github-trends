import React from 'react';
import {
  ThemeProvider as StyledThemeProvider,
  createGlobalStyle,
} from 'styled-components';
import { normalize } from 'styled-normalize';

import { ThemeGlobalStyle } from './ThemeGlobalStyle';
import { THEME } from './theme';

export const ThemeProvider: React.FC = ({ children }) => {
  return (
    <StyledThemeProvider theme={THEME}>
      <GlobalStyle />
      {children}
    </StyledThemeProvider>
  );
};

const GlobalStyle = createGlobalStyle`
  ${normalize}
  ${ThemeGlobalStyle}
`;
