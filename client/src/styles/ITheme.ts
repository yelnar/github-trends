interface IItemIndex {
  [index: number]: string;
}

export interface ITheme {
  colors: {
    primary: IItemIndex;
    dark: IItemIndex;
    gray: IItemIndex;
  };
  layout: {
    borderRadius: string;
  };
  fonts: {
    primary: string;
  };
}
