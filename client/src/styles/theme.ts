import { ITheme } from './ITheme';

export const THEME: ITheme = {
  colors: {
    dark: {
      1: '#0d1117',
      2: '#161b22',
      3: '#30363d',
    },
    gray: {
      1: '#9098a0',
      2: '#c9d1d9',
      3: '#cfcfcf',
    },
    primary: {
      1: '#58a6ff',
      2: '#384a78',
      3: '#42598d',
    },
  },
  layout: {
    borderRadius: '0.6rem',
  },
  fonts: {
    primary:
      '-apple-system,BlinkMacSystemFont,"Segoe UI",Helvetica,Arial,sans-serif,"Apple Color Emoji","Segoe UI Emoji"',
  },
};
