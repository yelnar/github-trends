import { API_ROUTE } from './API_ROUTE';

export const fetchRepositories = async () => {
  try {
    const response = await fetch(API_ROUTE + '/repositories');
    return await response.json();
  } catch (error) {
    console.error(error);
  }
};
