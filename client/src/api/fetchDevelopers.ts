import { API_ROUTE } from './API_ROUTE';

export const fetchDevelopers = async () => {
  try {
    const response = await fetch(API_ROUTE + '/developers');
    return await response.json();
  } catch (error) {
    console.error(error);
  }
};
