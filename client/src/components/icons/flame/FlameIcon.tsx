import styled from 'styled-components';

import { ReactComponent as FlameSvg } from './flame.svg';

export const FlameIcon = styled(FlameSvg)`
  fill: ${({ theme }) => theme.colors.gray[1]};
`;
