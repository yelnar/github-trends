import styled from 'styled-components';

import { ReactComponent as RepoSvg } from './repo.svg';

export const RepoIcon = styled(RepoSvg)`
  fill: ${({ theme }) => theme.colors.gray[1]};
`;
