import styled from 'styled-components';

import { ReactComponent as StarSvg } from './star.svg';

export const StarIcon = styled(StarSvg)`
  fill: ${({ theme }) => theme.colors.gray[1]};
`;
