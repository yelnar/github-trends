import styled from 'styled-components';

import { ReactComponent as ForkSvg } from './fork.svg';

export const ForkIcon = styled(ForkSvg)`
  fill: ${({ theme }) => theme.colors.gray[1]};
`;
