export interface ITestProps {
  testId?: string;
}

export interface ICommonProps extends ITestProps {
  className?: string;
}
