import React from 'react';
import styled from 'styled-components';

export const Header: React.FC = () => {
  return (
    <Root>
      <Title>Trending</Title>
      <Subtitle>
        See what the GitHub community is most excited about today.
      </Subtitle>
    </Root>
  );
};

const Root = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  flex-direction: column;
  background-color: ${({ theme }) => theme.colors.dark[2]};
  padding: 2.5rem;
`;

const Title = styled.h1`
  font-size: 2rem;
  font-weight: 500;
  color: ${({ theme }) => theme.colors.gray[2]};
  margin: 0;
`;

const Subtitle = styled.small`
  font-size: 1rem;
  margin-top: 0.5rem;
`;
