import React from 'react';
import styled from 'styled-components';

import { Header } from './components/header/Header';
import { Trends } from './shared/trends/Trends';

function App() {
  return (
    <Root>
      <Header />
      <Trends />
    </Root>
  );
}

const Root = styled.div``;

export default App;
