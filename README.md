# Minimal GitHub Trends Clone

This app shows trending GitHub repositories and developers.
It is similar to [GitHub trends](https://github.com/trending), although it is **not a full clone**.

It does not have most of the features that GitHub trends page offers.

Also, this app does not support responsive screen.

## Installation

Start server & client locally.

```bash
$ cd server
$ yarn install
# server starts on port 4000.
$ yarn start

$ cd ../client
$ yarn install
# client starts on port 3000.
$ yarn start 
```